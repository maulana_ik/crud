<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class ConfigController extends Controller
{
    //
    public function SignUp(){
        return view('form');
    }

    public function welcome(Request $request){
        //dd($request->all());
        $fnama = $request["fname"];
        $lnama = $request["lname"];
        return view('welcome_to', ["fnama" => $fnama, "lnama" => $lnama]);
    }

    public function create(){
        return view('posts.form');
    }

    public function store(Request $request){
        //dd($request ->all()) ;
        $query = DB::table('pertanyaan')->insert([
            "judul" => $request['judul'],
            "isi" => $request['isi']
        ]);

        return redirect('/pertanyaan/index')->with('success', 'Pertanyaan berhasil di post');
    }

    public function index(){
        $post = DB::table('pertanyaan')->get();
       // dd($post);
        return view('posts.data_pertanyaan', compact('post'));
    }
    public function show($id){
        $post = DB::table('pertanyaan') ->where('id', $id)->first();
        //dd($post);
        return view('posts.show', compact('post'));
    }
    public function edit($id){
        $post = DB::table('pertanyaan') ->where('id', $id)->first();
        return view('posts.edit', compact('post'));
    }
    public function update($id, Request $request){
        $query= DB::table('pertanyaan')
                    ->where('id', $id)
                    ->update([
                        'judul' => $request['judul'],
                        'isi' => $request['isi']
                    ]);
                    return redirect('/pertanyaan/index');
    }
    public function destroy($id){
        $query = DB::table('pertanyaan')->where('id', $id)->delete();
        return redirect('/pertanyaan/index');
    }
}
