<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('layout.content');
});

Route::get('/form', function(){
    return view('form');
});


Route::post('/welcome1', 'ConfigController@welcome');
Route::get('/data-tables', function(){
    return view('layout.table');
});
Route::get('/pertanyaan/index', 'ConfigController@index');
Route::get('/pertanyaan/create', 'ConfigController@create');
Route::post('/pertanyaan', 'ConfigController@store');
Route::get('/pertanyaan/{pertanyaan_id}', 'ConfigController@show');
Route::get('/pertanyaan/{pertanyaan_id}/edit', 'ConfigController@edit');
Route::put('/pertanyaan/{pertanyaan_id}', 'ConfigController@update');
Route::delete('/pertanyaan/{pertanyaan_id}', 'ConfigController@destroy');