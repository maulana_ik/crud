@extends('master')

@section('show')
<div class="card card-primary card-outline">
              <div class="card-header">
                <h5 class="m-0">#ASK {{$post->judul}}</h5>
              </div>
              <div class="card-body">
                <h6 class="card-title">{{$post->isi}}</h6>
              </div>
            </div>
@endsection