@extends('master')

@section('form')
<div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Form Pertanyaan</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="/pertanyaan/{{$post->id}}" method="POST">
              @csrf
              @method('PUT')
                <div class="card-body">
                  <div class="form-group">
                    <label for="judul">Judul</label>
                    <input type="text" class="form-control" id="judul" placeholder="Judul Pertanyaan" name="judul"
                    value="{{ old('judul', $post->judul)}}">
                  </div>
                  <div class="form-group">
                        <label>Pertanyaan</label>
                        <input type="text"class="form-control" rows="3" placeholder="Enter ..." name="isi" id="isi"
                        value="{{ old('isi', $post->isi)}}"></input> 
                      </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Kirim</button>
                </div>
              </form>
            </div>
@endsection



