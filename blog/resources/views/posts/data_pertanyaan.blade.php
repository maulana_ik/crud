@extends('master')

@section('list')
<div class="card">
              <div class="card-header">
                <h3 class="card-title">List Pertanyaan</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
              @if(session('success'))
                <div class="alert alert-success">
                  {{session('success')}}
                </div>
              @endif
                <table class="table table-bordered">
                  <thead>                  
                    <tr>
                      <th style="width: 10px"> <i class="fa fa-check-square"></i> </th>
                      <th>Judul</th>
                      <th>Pertanyaan</th>
                      <th style="width: 240px">Action</th>
                    </tr>
                  </thead>
                  <tbody>
               
                    @foreach($post as $key => $post)
                    <tr>
                        <td> {{ $key + 1 }}</td>
                        <td> {{ $post->judul}}</td>
                        <td> {{ $post->isi}}</td>
                        <td>
                            
                            <a href="/pertanyaan/{{$post->id}}" class="btn btn-info ">Details</a>
                            <a href="/pertanyaan/{{$post->id}}/edit" class="btn btn-primary ">Edit</a>
                            <form action="/pertanyaan/{{$post->id}}" method="POST" style="display:inline;">
                            @csrf
                            @method('DELETE')
                            <input type="submit"class="btn btn-danger " value="Delete">
                            </form>
                        </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>  
@endsection